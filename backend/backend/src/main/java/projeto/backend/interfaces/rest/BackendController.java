package projeto.backend.interfaces.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import projeto.backend.application.ColaboradorService;
import projeto.backend.domain.Colaborador;
import projeto.backend.domain.Regras;

import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
public class BackendController {

    private final ColaboradorService service;

    @RequestMapping(value = "colaborador/{cpf}",
            method = RequestMethod.GET)
    public ResponseEntity getOne(@PathVariable(value = "cpf") String cpf) {
        try {
            service.initialize();

            Optional<Colaborador> colaborador = service.getColaborador(cpf);

            if(colaborador.isPresent()) {
                return ResponseEntity.ok(colaborador.get());
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception ex) {
            return ResponseEntity.status(500).body("Error in get colaborador");
        }
    }

    @RequestMapping(value = "colaborador",
            method = RequestMethod.GET)
    public ResponseEntity getAll() {
        try {
            service.initialize();
            List<Colaborador> colaboradores = service.getAll();

            return ResponseEntity.ok(colaboradores);
        } catch (Exception ex) {
            return ResponseEntity.status(500).body("Error in get colaborador");
        }
    }

    @RequestMapping(value = "valid",
            method = RequestMethod.GET)
    public ResponseEntity isValid() {
        try {
            service.initialize();

            return ResponseEntity.ok(service.valid());
        } catch (Exception ex) {
            return ResponseEntity.status(500).body("Error in get colaborador");
        }
    }

    @RequestMapping(value = "delete/{cpf}",
            method = RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable("cpf") String cpf) {
        try {
            service.initialize();

            if(service.delete(cpf)) {
                return ResponseEntity.ok("");
            } else {
                return ResponseEntity.badRequest().body("CPF not exist or is invalid");
            }

        } catch (Exception ex) {
            return ResponseEntity.status(500).body("Error in delete");
        }
    }

    @RequestMapping(value = "colaborador",
            method = RequestMethod.POST)
    public ResponseEntity post(@RequestBody Colaborador colaborador) {
        try {

            if(colaborador.getIdade() == 0) {
                return ResponseEntity.badRequest().body("Required field idade");
            }

            Regras regras = service.valid();

            if( colaborador.getIdade() > 65 && !regras.isMaior65() ) {
                return ResponseEntity.badRequest().body("Greater than 65 old is biggest than 20%");
            }

            if( colaborador.getIdade() < 18 && !regras.isMenor18() ) {
                return ResponseEntity.badRequest().body("Less than 18 old is biggest than 20%");
            }

            if(!colaborador.getSetor().equalsIgnoreCase(service.toSetor(colaborador.getSetor()))) {
                return ResponseEntity.badRequest().body("Setor is invalid!");
            }
            Colaborador colab = service.update(colaborador);

            if(colab != null) {
                return ResponseEntity.status(201).build();
            }

            return ResponseEntity.status(500).body("Error in post");

        } catch (Exception ex) {
            return ResponseEntity.status(500).body("Error in post colaborador");
        }
    }
}
