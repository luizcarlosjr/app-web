package projeto.backend.application;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import projeto.backend.domain.Colaborador;
import projeto.backend.domain.Regras;
import projeto.backend.domain.Setor;
import projeto.backend.repository.ColaboradorRepository;
import projeto.backend.repository.SetorRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ColaboradorService {

    private final ColaboradorRepository repository;
    private final SetorRepository setorRepository;

    public Optional<Colaborador> getColaborador(String cpf) {

        try {
            return repository.findById(cpf);
        } catch (Exception ex) {
            ex.printStackTrace();
            return Optional.empty();
        }

    }

    public List<Colaborador> getAll() {

        try {
            return repository.findAll();
        } catch (Exception ex) {
            ex.printStackTrace();
            return new ArrayList<>();
        }

    }

    public Colaborador update(Colaborador colaborador) {

        try {
            return repository.save(colaborador);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

    }

    public boolean delete(String cpf) throws Exception {

        try {
            Optional<Colaborador> colaborador = repository.findById(cpf);

            if( colaborador.isPresent()) {
                repository.delete(colaborador.get());
                return true;
            }

            return false;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception("Error in delete");
        }


    }

    public String toSetor(String name) {
        try {

            Setor setor = setorRepository.findByName(name);

            if( setor == null) {
                return null;
            }

            return setor.getName();
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public Regras valid() {
        try {

            long count = repository.count();

            long qtd = count/5;

            if(qtd == 0) {
                return new Regras(false, false);
            }

            boolean maior65 = false;
            boolean menor18 = false;

            List<Colaborador> colaboradores65 = repository.findByIdadeGreaterThan(65);
            List<Colaborador> colaboradores18 = repository.findByIdadeLessThan(18);

            if(colaboradores18.size() < qtd) {
                menor18 = true;
            }

            if(colaboradores65.size() < qtd) {
                maior65 = true;
            }

            return new Regras(maior65, menor18);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public void initialize() throws Exception {

        try {
            List<Setor> setores = new ArrayList<>();

            setores.add(new Setor("1", "TI"));
            setores.add(new Setor("2", "RH"));
            setores.add(new Setor("3", "Marketing"));
            setores.add(new Setor("4", "Administração"));
            setores.add(new Setor("5", "Gerência"));

            setorRepository.deleteAll();
            setorRepository.saveAll(setores);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception("Error in repository");
        }

    }


}
