package projeto.backend.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@Builder
@Document(collection = "colaborador")
public class Colaborador {

    @Id
    @Indexed(name = "cpf")
    private String cpf;

    private String nome;

    private String email;

    private String telefone;

    private String celular;

    private String setor;

    @Indexed(name = "idade")
    private int idade;
}
