package projeto.backend.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Regras {

    private boolean maior65;

    private boolean menor18;
}
