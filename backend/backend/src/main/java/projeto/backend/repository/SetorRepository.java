package projeto.backend.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import projeto.backend.domain.Setor;

@Repository
public interface SetorRepository extends MongoRepository<Setor, String> {

    Setor findByName(@Param("name") String name);
}
