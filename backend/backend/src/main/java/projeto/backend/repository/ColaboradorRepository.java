package projeto.backend.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import projeto.backend.domain.Colaborador;

import java.util.List;

@Repository
public interface ColaboradorRepository extends MongoRepository<Colaborador, String> {

    List<Colaborador> findByIdadeGreaterThan(@Param("idade") int idade);
    List<Colaborador> findByIdadeLessThan(@Param("idade") int idade);
}
