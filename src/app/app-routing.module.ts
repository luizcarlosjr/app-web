import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Listar } from './colaborador/listar/listar';
import { Colab } from './colaborador/colab';
import { Detalhes } from './colaborador/detalhes/detalhes';


const routes: Routes = [
  {path: '', component: Colab},
  {path: 'colaborador', component: Listar},
  {path: 'colaborador/:id', component: Detalhes},
  {path: 'colaborador/search/:name', component: Detalhes}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
