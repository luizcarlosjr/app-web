import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Colab } from './colaborador/colab';
import { Listar } from './colaborador/listar/listar';
import { Detalhes } from './colaborador/detalhes/detalhes';
import { FormsModule }   from '@angular/forms';

import {NgxMaskModule, IConfig} from 'ngx-mask'

export const options: Partial<IConfig> | (() => Partial<IConfig>) = undefined;

@NgModule({
  declarations: [
    AppComponent,
    Colab,
    Listar,
    Detalhes
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    NgxMaskModule.forRoot(options)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
