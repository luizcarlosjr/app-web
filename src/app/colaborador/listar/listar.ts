import { Component, OnInit } from '@angular/core';
import { Colaborador } from 'src/model/Colaboradores';
import { Setor } from 'src/model/setor';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'listar-colab',
  templateUrl: './listar.html',
  styleUrls: ['./listar.scss']
})
export class Listar implements OnInit {

  constructor(private active: ActivatedRoute) { }

  colaboradores = [];
  lista = true;

  setores = []
  setor = { nome: "Selecione um setor" };
  nome = "";
  email = "";
  tel = "";
  cel = "";
  cpf = "";
  value;

  colabs = [];

  edit = false;

  ngOnInit() {
    console.log(this.active)
    if (this.active.routeConfig.path === "colaborador/novo") {
      this.lista = false;
    }
    this.colaboradores.push(new Colaborador("Luiz Carlos", "luiz@email.com", Setor.Gerência, "12312312311", "2112341234", "21912341234"))

    this.colabs = this.colaboradores;
    this.setores.push({ nome: Setor.RH })
    this.setores.push({ nome: Setor.TI })
    this.setores.push({ nome: Setor.Marketing })
    this.setores.push({ nome: Setor.Administração })
    this.setores.push({ nome: Setor.Gerência })
  }

  salvar() {
    if (this.nome == "") {
      alert('Nome obrigatório!');
      return;
    }
    if (this.email == "") {
      alert('Email obrigatório!');
      return;
    }
    if (this.tel == "") {
      alert('Telefone obrigatório!');
      return;
    }
    if (this.cpf == "") {
      alert('CPF obrigatório!');
      return;
    }

    if (this.setor.nome == "Selecione um setor") {
      alert('Setor obrigatório!');
      return;
    }

    let setor: Setor;

    switch (this.setor.nome) {
      case Setor.TI:
        setor = Setor.TI;
        break;
      case Setor.RH:
        setor = Setor.RH;
        break;
      case Setor.Marketing:
        setor = Setor.Marketing;
        break;
      case Setor.Gerência:
        setor = Setor.Gerência;
        break;
      case Setor.Administração:
        setor = Setor.Administração;
        break;
    }

    let ok = false;
    this.colaboradores.forEach((colab: Colaborador) => {
      if (colab.cpf == this.cpf) {
        colab.setor = setor;
        colab.email = this.email;
        colab.celular = this.cel;
        colab.nome = this.nome;
        colab.telefone = this.tel;

        this.lista = true;

        this.clear()
        ok = true;
      }
    })

    if (!ok) {
      this.colaboradores.push(new Colaborador(this.nome, this.email, setor, this.cpf, this.tel, this.cel));
      this.lista = true;

      this.edit = false;
      this.clear()
    }

    this.colabs = this.colaboradores;


  }

  cancelar() {
    this.clear()
    this.lista = true;
    this.edit = false;
  }

  editar(c: Colaborador) {
    this.edit = true;
    this.nome = c.nome
    this.tel = c.telefone
    this.cel = c.celular
    this.cpf = c.cpf
    this.email = c.email
    this.setor = { nome: c.setor }

    console.log(this.setor)
    this.lista = false;
  }

  private clear() {
    this.nome = ""
    this.tel = ""
    this.cel = ""
    this.cpf = ""
    this.email = ""
    this.setor = { nome: "Selecione um setor" }
  }

  filtrar(value: string) {
    this.colaboradores = this.colabs;
    
    if(value == undefined || value.trim() == "") {
      return;
    }

    this.colaboradores = []

    this.colabs.forEach( (c: Colaborador) => {
      if(c.nome.toUpperCase().includes(value.toUpperCase())) {
        console.log(c)
        this.colaboradores.push(c)
      }
    })
  }

}