export enum Setor {

    TI = "TI",
    RH = "RH",
    Marketing = "Marketing",
    Administração = "Administração",
    Gerência = "Gerência"
}