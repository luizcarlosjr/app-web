import { Setor } from './setor';

export class Colaborador {
    
    nome: string;
    email: string;
    setor: Setor;
    telefone: string;
    celular: string;
    cpf: string;


    constructor(nome: string, email: string, setor: Setor, cpf: string, telefone: string, celular: string) {
        this.nome = nome;
        this.email = email;
        this.setor = setor;
        this.telefone = telefone;
        this.celular = celular;
        this.cpf = cpf;
    }
}